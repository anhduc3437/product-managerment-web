<?php

namespace App\Services;

use App\Models\Category;
use App\Repositories\ProductRepository;
use Image;


/**
 * Class ProductService
 * @package App\Services
 */
class ProductService extends BaseService
{
    /**
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * ProductService constructor.
     * @param ProductRepository $equipRepository
     */
    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function getAll()
    {
        return $this->productRepository->all();
    }

    public function getById($id)
    {
        return $this->productRepository->find($id);
    }

    public function getActiveProductByCategory($category)
    {
        if (!empty($category)) {
            $products = $this->productRepository->getActiveProductByCategory($category);
        } else {
            $products = $this->productRepository->getActiveProduct();
        }
        return $products;
    }

    public function create($data)
    {
        if (array_key_exists('image', $data)) {
            // get image file
            $image = $data['image'];

            $image_resize = Image::make($image->getRealPath());
            $image_resize->resize(255, 164);

            // save relative path of img to $data['image']
            $filepath = 'uploads/products/' . $image->getClientOriginalName();

            // save img to public folder
            $image_resize->save(public_path($filepath));
        } else {
            $filepath = 'uploads/products/no-image.png';
        }

        $data['filepath'] = $filepath;
        unset($data['image']);
        return $this->productRepository->create($data);
    }

    public function update($id, $data)
    {
        if (!array_key_exists('status', $data)) {
            $data = array_merge($data, ['status' => '0']);
        }
        return $this->productRepository->update($id, $data);
    }

    public function delete($id)
    {
        return $this->productRepository->delete($id);
    }
}
