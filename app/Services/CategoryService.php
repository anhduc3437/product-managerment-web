<?php

namespace App\Services;

use App\Repositories\CategoryRepository;

/**
 * Class CategoryService
 * @package App\Services
 */
class CategoryService extends BaseService
{
    /**
     * @var categoryRepository
     */
    protected $categoryRepository;

    /**
     * CategoryService constructor.
     * @param CategoryRepository $equipRepository
     */
    public function __construct(CategoryRepository $categoryRepository) 
    {
        $this->categoryRepository    = $categoryRepository;
    }

    public function getAll()
    {
        return $this->categoryRepository->all();
    }

    public function getById($id)
    {
        return $this->categoryRepository->find($id);
    }

    public function getActiveCategory()
    {
        return $this->categoryRepository->getActiveCategory();
    }

    public function create($data)
    {
        return $this->categoryRepository->create($data);
    }
    
    public function update($id, $data)
    {
        if (!array_key_exists('status', $data)) {
            $data = array_merge($data, ['status' => '0']);
        }
        return $this->categoryRepository->update($id, $data);
    }

    public function delete($id)
    {
        return $this->categoryRepository->delete($id);
    }
}