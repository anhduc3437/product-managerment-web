<?php

namespace App\Services;

use App\Repositories\RepositoryInterface;

/**
 * Class BaseService
 * @package App\Services
 */
abstract class BaseService
{
    /**
     * @var RepositoryInterface
     */
    protected $repository;
}
