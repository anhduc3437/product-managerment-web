<?php

namespace App\Repositories;


class CategoryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    public function getAll()
    {
        return $this->model->all();
    }
    
    public function getById($id)
    {
        return $this->model->find($id);
    }

    public function getActiveCategory()
    {
        return $this->model->where('status', '1')->get();
    }

    public function create($data)
    {
        return $this->model->create($data);
    }

    public function update($id, $data)
    {
        return $this->model->find($id)->update($data);
    }

    public function delete($id)
    {
        return $this->model->find($id)->delete();
    }
}