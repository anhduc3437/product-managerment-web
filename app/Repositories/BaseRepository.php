<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

abstract class BaseRepository
{
    /** @var Model */
    protected $model;

    /**
     * @var array
     */
    protected $fieldSearchable = ['name'];

    private $modelPath = "App\Models\\";

    public function __construct()
    {
        // auto setup $model variable
        if (empty($this->model)) {
            $search = 'Repository';
            $repoName = class_basename($this);
            $position = strpos($repoName, $search);
            if ($position !== false) {
                $modelName = $this->modelPath.str_replace($search, '', $repoName);

                $this->model = resolve($modelName);
            }
        }
    }

    /**
     * This magic method to call function from model when it doesn't appear in BaseRepository class
     *
     * @param $name
     * @param $arguments
     * @return Model
     */
    public function __call($name, $arguments)
    {
        return $this->model->{$name}(...$arguments);
    }

    /**
     * Creates or updates the given resource.
     *
     * @param array $rows
     * @param array $exclude The attributes to exclude in case of update.
     *
     * @return string
     */
    public function createOrUpdate(array $rows, array $exclude = [])
    {
        // We assume all rows have the same keys so we arbitrarily pick one of them.
        $columns = array_keys($rows[0]);

        $columnsString  = implode('`,`', $columns);
        $values         = $this->buildSqlValuesFrom($rows);
        $updates        = $this->buildSqlUpdatesFrom($columns, $exclude);

        // Laravel 8 and PHP removed array_flatten method, so we have to do it trickily
        $params = Arr::flatten($rows);

        $query = "insert into {$this->model->getTable()} (`{$columnsString}`) values {$values} on duplicate key update {$updates}";

        \DB::statement($query, $params);

        return $query;
    }

    /**
     * Build proper SQL string for the values.
     *
     * @param array $rows
     * @return string
     */
    protected function buildSqlValuesFrom(array $rows)
    {
        $values = collect($rows)->reduce(function ($valuesString, $row) {
            return $valuesString .= '(' . rtrim(str_repeat("?,", count($row)), ',') . '),';
        }, '');

        return rtrim($values, ',');
    }

    /**
     * Build proper SQL string for the on duplicate update scenario.
     *
     * @param       $columns
     * @param array $exclude
     * @return string
     */
    protected function buildSqlUpdatesFrom($columns, array $exclude)
    {
        $updateString = collect($columns)->reject(function ($column) use ($exclude) {
            return in_array($column, $exclude);
        })->reduce(function ($updates, $column) {
            return $updates .= "`{$column}`=VALUES(`{$column}`),";
        }, '');

        return trim($updateString, ',');
    }

    /**
     * get data search for select 2
     *
     * @param array $condition
     * @param string $searchField
     * @param integer $pageSize
     * @param array $exclude (list exclude id) 
     * @return json 
     */
    public function getAutocomplete($condition = [], $searchField = 'name', $pageSize = 30)
    {
        if(!in_array($searchField, $this->fieldSearchable)){
            $results = [
                    "items" => [],
                    "pagination" => [
                        "more" => false
                    ]
                ];

            return response()->json($results);
        }

        $page       = $condition['page'] ?? 1;
        $exclude    = $condition['exclude'] ?? [];
        $offset     = ($page - 1) * $pageSize;

        $query  = $this->model->where($searchField, 'LIKE', '%' . ($condition['q'] ?? ''). '%')->orderBy($searchField);
        if(is_array($exclude) && count($exclude) > 0){
            $query->whereNotIn('id', $exclude);
        } 

        $dataResult = (clone $query)->skip($offset)
                        ->take($pageSize)
                        ->selectRaw('id, '.$searchField.' as text')
                        ->get();

        $count = (clone $query)->count();

        $endCount   = $offset + $pageSize;
        $morePages  = $count > $endCount;

        $results = array(
                "items" => $dataResult->toarray(),
                "pagination" => array(
                    "more" => $morePages
                )
          );

        return response()->json($results);
    }
}
