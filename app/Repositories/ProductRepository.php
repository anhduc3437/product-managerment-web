<?php

namespace App\Repositories;

use App\Models\Category;

class ProductRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'category',
        'price',
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    public function getAll()
    {
        return $this->model->all();
    }

    public function getById($id)
    {
        return $this->model->find($id);
    }

    public function getActiveProduct()
    {
        return $this->model->where('status', '1')->get();
    }

    public function getActiveProductByCategory($category)
    {
        return Category::find($category)->products()->where('status', '1')->get();
    }

    public function create(array $input)
    {
        return $this->model->create($input);
    }

    public function update($id, $data)
    {
        return $this->model->find($id)->update($data);
    }

    public function delete($id)
    {
        return $this->model->find($id)->delete();
    }
}