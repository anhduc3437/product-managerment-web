<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'id',
        'name',
        'category_id',
        'price',
        'amount',
        'filepath',
        'status',
    ];

    public function getCategoryNameAttribute()
    {
        return $this->category->name;
    }

    public function getStatusAttribute($status)
    {
        return $status ? 'Active' : 'Inactive';
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
