<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'id',
        'name',
        'status',
    ];

    public function getStatusAttribute($status)
    {
        return $status ? 'Active' : 'Inactive';
    }
    
    public function products()
    {
        return $this->hasMany(Product::class);
    }
}
