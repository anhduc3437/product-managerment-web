<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// })->name('/');

Route::get('/', [HomeController::class, 'index'])->name('home');



Auth::routes(['verify' => true]);

Route::prefix('admin')
->middleware(['auth', 'check.admin'])
->group(function () {
    Route::get('/login', function () {
        return redirect('/login');
    });

    Route::resource('products', ProductController::class);
    Route::resource('categories', CategoryController::class);
});