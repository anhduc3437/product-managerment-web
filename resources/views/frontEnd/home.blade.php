@extends('layouts.frontEnd.app')
@section('content')

<div class="container">

    <div class="row main-content">
        <div class="col-7 c7">
            <div class="row h1-div">
                <h1>イベント</h1>
                <div class="line"></div>
            </div>
            <div>
                <h5>Wix（ウィックス）のSEO設定をしよう！検索順位をアップして集客を強化</h5>
                <small>スタートアップ等が、コストをかけずにすぐにWebサイトを準備したい場合、WIxは手軽に使えるホームページ作成サービスとして便利です。しかし、無料もしくは安価で手軽に使える反面、ホームページとして重要な集客力がどれほどあるのかは気になるところでしょう。</small>
            </div>

        </div>

        <div class="col-5 c5">
            <div class="main-img">
                <img src="{{ asset('images/main-img.jpg') }}" class="css-class big-img" alt="alt text">

            </div>
        </div>
    </div>
</div>

<div class="container">
    @for ($i = 0; $i < $products->count(); $i+=4)
        <div class="row list-product">
            @for ($j = $i; $j < $i+4 && $j < $products->count(); $j++)
                <div class="col-3">
                    <img src="{{ asset($products[$j]->filepath) }}" class="css-class small-img" alt="alt text">
                    <div class="detail">
                        <p class="pp">{{ $products[$j]->name }}</p>
                        <p class="price pp">{{ $products[$j]->price == null ? 'No price' : $products[$j]->price }}</p>
                    </div>
                </div>
                @endfor
        </div>
        @endfor

</div>

@endsection