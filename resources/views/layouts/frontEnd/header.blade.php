<div class="menu">
    <div class="container ctn-menu">
        <a href="{{ route('home') }}" class="cat-link">
            <div class="menu-item">All product</div>
        </a>
        @foreach ($categories as $category)
        <a href="{{ route('home', ['category'=> $category->id]) }}" class="cat-link">
            <div class="menu-item">{{ $category->name }}</div>
        </a>
        @endforeach
    </div>
</div>