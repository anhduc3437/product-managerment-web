<!-- Scripts -->
<script src="{{ asset('js/app.js') }}" defer></script>

<div class="menu">
    <a class="navbar-brand nav-link" href="{{ url('/') }}">
        {{ config('app.name', 'Laravel') }}
    </a>
    <div class="container ctn-menu">
        <a class="col-4 menu-item" href="{{ route('categories.index') }}"> MANAGER CATEGORIES</a>
        <a class="col-4 menu-item" href="{{ route('products.index') }}">MANAGER PRODUCTS</a>
    </div>

    <div id="navbarSupportedContent">
        <ul class="navbar-nav ms-auto">
            <li class="nav-item dropdown">
                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    {{ Auth::user()->name }}
                </a>

                <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                </div>
            </li>
        </ul>
    </div>
</div>