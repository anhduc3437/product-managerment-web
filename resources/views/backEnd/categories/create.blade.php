@extends('layouts.backEnd.app')
@section('content')

<form action="{{ route('categories.store') }}" method="POST" class="container cr-up-form">

    @csrf
    <p class="form-name">Create category</p>

    <div class="form-group">
        <label for="name" class="required">Name of category</label>
        <input type="text" class="form-control input-name" id="name" name="name" required>
    </div>

    <div class="form-group check-status">
        <input class="status" type="checkbox" name="status" value="1">&nbsp; Active
    </div>

    <div class="form-group container can-sub-btn">
        <a href="{{ route('categories.index') }}" class="btn can-btn">Cancel</a>
        <button type="submit" class="btn btn-primary add-btn">
            Submit
            @method('POST')
        </button>
    </div>
</form>

@endsection