@extends('layouts.backEnd.app')
@section('content')

<form action="{{ route('categories.update', ['category' => $category->id]) }}" method="POST" class="container cr-up-form">

    @csrf
    <p class="form-name">Edit category</p>

    <div class="form-group">
        <label for="name" class="required">Name of category</label>
        <input type="text" class="form-control input-name" id="name" value="{{$category->name}}" name="name" required/>
    </div>

    <div class="form-group check-status">
        @if ($category->status == 'Active')
            <input class="status" type="checkbox" name="status" value="1" checked> Active
        @else
            <input class="status" type="checkbox" name="status" value="1"> Active
        @endif
    </div>

    <div class="form-group container can-sub-btn">
        <a href="{{ route('categories.index') }}" class="btn can-btn">Cancel</a>
        <button type="submit" class="btn btn-primary add-btn">
            Submit
            @method('PUT')
        </button>
    </div>
</form>

@endsection