@extends('layouts.backEnd.app')
@section('content')

<div class="container">
    <div class="container head-table">
        <div class="table-name">
            List of categories
        </div>
        <div>
            <div>
                <a href="{{ route('categories.create') }}" class="btn btn-primary add-btn">Add new category</a>
            </div>
        </div>
    </div>

    <div class="body-table">
        <table class="table container ctn-content">
            <thead>
                <tr>
                    <th> ID </th>
                    <th> Name of category </th>
                    <th> Status </th>
                    <th> Active </th>
                </tr>
            </thead>
            <tbody>
                @foreach($categories as $category)
                <tr>
                    <td class="text-left col-2">{{$category->id}}</td>
                    <td class="text-left col-6">{{$category->name}}</td>
                    <td class="text-left col-2 {{$category->status}}">{{$category->status}}</td>
                    <td class="text-left col-2 ed-btn">
                        <a href="{{ route('categories.edit', ['category' => $category->id]) }}" class="edit-btn">Edit</a>
                        <form action="{{route('categories.destroy', ['category' => $category->id])}}" method="post">
                            <button type="submit" class="delete-btn" onclick="return confirm('Are you sure you want to delete this item?')">
                                Delete
                                @csrf
                                @method('DELETE')
                            </button>
                        </form>

                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

@endsection