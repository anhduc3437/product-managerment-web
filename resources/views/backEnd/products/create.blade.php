@extends('layouts.backEnd.app')
@section('content')

<form action="{{ route('products.store') }}" method="POST" class="container cr-up-form" enctype="multipart/form-data">

    @csrf
    <p class="form-name">Create product</p>

    <div class="form-group">
        <label for="name" class="required">Name of product</label>
        <input type="text" class="form-control input-name" id="name" name="name" required>
    </div>

    <div class="form-group">
        <label for="category_id" class="required">Category</label>
        <select class="form-control input-name" id="category_id" name="category_id" required>
            <option hidden selected></option>
            @foreach ($categories as $category)
            <option value="{{$category->id}}">{{$category->name}}</option>
            @endforeach
        </select>
    </div>

    <div class="amount-price">
        <div class="form-group">
            <label for="price">Price</label>
            <input type="text" class="form-control input-name amount" id="price" name="price">
        </div>
        <div>
            <label for="amount">Amount</label>
            <input type="text" class="form-control input-name price" id="amount" name="amount">
        </div>
    </div>

    <div class="form-group">
        <label for="image">Image of product</label>
        <input type="file" class="form-control input-name" id="image" name="image">
    </div>

    <div class="form-group check-status">
        <input type="checkbox" class="status" name="status" value="1"> Active
    </div>

    <div class="form-group container can-sub-btn">
        <a href="{{ route('products.store') }}" class="btn can-btn">Cancel</a>
        <button type="submit" class="btn btn-primary add-btn">
            Submit
        </button>
    </div>
</form>

@endsection