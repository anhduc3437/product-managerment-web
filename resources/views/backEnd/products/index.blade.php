@extends('layouts.backEnd.app')
@section('content')

<div class="container">
    <div class="container head-table">
        <div class="table-name">
            List of products
        </div>
        <div>
            <div>
                <a href="{{ route('products.create') }}" class="btn btn-primary add-btn">Add new product</a>
            </div>
        </div>
    </div>

    <div class="body-table">
        <table class="table container ctn-content">
            <thead>
                <tr>
                    <th> ID </th>
                    <th> Name of product </th>
                    <th> Status </th>
                    <th> Active </th>
                </tr>
            </thead>
            <tbody>
                @foreach($products as $product)
                <tr>
                    <td class="text-left col-2">{{$product->id}}</td>
                    <td class="text-left col-6">{{$product->name}}</td>
                    <td class="text-left col-2 {{$product->status}}">{{$product->status}}</td>
                    <td class="text-left col-2 ed-btn">
                        <a href="{{ route('products.edit', ['product' => $product->id]) }}" class="edit-btn">Edit</a>
                        <form action="{{ route('products.destroy', ['product' => $product->id]) }}" method="post">
                            <button type="submit" class="delete-btn" onclick="return confirm('Are you sure you want to delete this item?')">
                                Delete
                                @csrf
                                @method('DELETE')
                            </button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

@endsection