@extends('layouts.backEnd.app')
@section('content')

<form action="{{ route('products.update', ['product' => $product->id]) }}" method="POST" class="container cr-up-form" enctype="multipart/form-data">

    @csrf
    <p class="form-name">Edit product</p>

    <div class="form-group">
        <label for="name" class="required">Name of product</label>
        <input type="text" class="form-control input-name" id="name" name="name" value="{{$product->name}}" required>
    </div>

    <div class="form-group">
    <label for="category_id" class="required">Category</label>
        <select class="form-control input-name" id="category_id" name="category_id" required>
            <option hidden selected value="{{$product->category_id}}">{{$product->categoryName}}</option>
            @foreach ($categories as $category)
            <option value="{{$category->id}}">{{$category->name}}</option>
            @endforeach
        </select>
    </div>

    <div class="amount-price">
        <div class="form-group">
            <label for="price">Price</label>
            <input type="text" class="form-control input-name amount" id="price" name="price" value="{{$product->price}}" />
        </div>
        <div>
            <label for="amount">Amount</label>
            <input type="text" class="form-control input-name price" id="amount" name="amount" value="{{$product->amount}}" />
        </div>
    </div>

    <div class="form-group">
        <label for="image">Image of product</label>
        <input type="file" class="form-control input-name" id="image" name="image" />
    </div>

    <div class="form-group check-status">
        @if ($product->status == 'Active')
        <input class="status" type="checkbox" name="status" value="1" checked> Active
        @else
        <input class="status" type="checkbox" name="status" value="1"> Active
        @endif
    </div>

    <div class="form-group container can-sub-btn">
        <a href="{{ route('products.index') }}" class="btn can-btn">Cancel</a>
        <button type="submit" class="btn btn-primary add-btn">
            Submit
            @csrf
            @method('PUT')
        </button>
    </div>
</form>

@endsection